<?php
function my_autoloader($class)
{
    // echo "$file_path included !<br>";
    $file_path = "{$class}.php";
    if (file_exists($file_path)) {
        include_once "$file_path";
    } else {
        // log "$file_path"
    }
}


spl_autoload_register("my_autoloader");
