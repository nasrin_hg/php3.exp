<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);


define("DB_HOST", '127.0.0.1');
define("DB_NAME", '7task');
define("DB_USER", 'root');
define("DB_PASSWORD", '');

const TASK_TABLE = 'tasks';


$task_statuses = [1 => 'draft', 2 => 'todo', 3 => 'done', 4 => 'archive'];

$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// Check connection
if ($conn->connect_errno) {
 die("Connection failed: " . $conn->connect_error);
}
// echo "Connected successfully";
