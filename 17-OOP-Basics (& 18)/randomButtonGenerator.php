<?php
include_once "Classes/Button.class.php";

// null coalescing operator
// standardization
$buttonCount = $_GET['numButtons'] ?? 0;

if ($buttonCount > 0 && is_numeric($buttonCount)) {
    for ($i = 1; $i <= $buttonCount; $i++) {
        $btn1 = new Button("Button $i");  /// instantiation
        $btn1->addStyle("font-size: " . $i * 5);
        $btn1->addStyle("color: rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")");
        $btn1->render();
    }
}
