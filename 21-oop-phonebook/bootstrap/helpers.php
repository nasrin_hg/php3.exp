<?php


function asset($asset)
{
    $asset_path = BASE_URL . "themes/" . ACTIVE_THEME . "/$asset";
    return $asset_path;
}

function site_url($uri)
{
    return BASE_URL . $uri;
}

function redirect($uri)
{   // fix this please
    $url = site_url($uri);
    header("Location: $url");
    die();
}


function isPostRequest()
{
    return ($_SERVER['REQUEST_METHOD'] == 'POST');
}

function isGetRequest()
{
    return ($_SERVER['REQUEST_METHOD'] == 'GET');
}

function get_config($key)
{
    $configs = array(
        "db" => [
            // required
            'database_type' => 'mysql',
            'database_name' => 'phonebook',
            'server' => 'localhost',
            'username' => 'root',
            'password' => '',

            // [optional]
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
            'port' => 3306,

            // [optional] Enable logging (Logging is disabled by default for better performance)
            'logging' => true,

            // [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
            'option' => [
                PDO::ATTR_CASE => PDO::CASE_NATURAL
            ],

            // [optional] Medoo will execute those commands after connected to the database for initialization
            'command' => [
                'SET SQL_MODE=ANSI_QUOTES'
            ]
        ],
        "notification" => [
            // notif config
        ]
    );
    return $configs[$key];
}
