<?php
include_once "../init.php";


// [$email, $password] = [1, 2];

if ($_SERVER['REQUEST_METHOD'] == "POST") {
 $email = trim($_POST['email']);
 $password = $_POST['password'];

 if (login($email, $password)) {
  echo "Login Successfull!<br>";
  echo "Redirecting to Home Page ...";
  jsRedirect(BASE_URL, 2);
 } else {
  echo "Incorrect User or Password";
 }
}
